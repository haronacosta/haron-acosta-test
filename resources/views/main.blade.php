@extends('layouts.app')

@section('title', 'Lista de Post')

@section('content')

<div class="container">
  @if (session('success'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    {{ session('success') }}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  @endif

  <div class="h2 my-5 text-center"> Listado de Posts</div>
  <div class="row">
    @foreach ($posts as $post)
    <div class="col-lg-6 mb-4">
      <div class="card h-100">
        <div class="card-body">
          <h4 class="card-title">
            <a href="{{ url('/post/' . $post['id']) }}">{{ $post['title'] }}</a>
          </h4>
          <p class="card-text">{{$post['content']}}</p>
        </div>
      </div>
    </div>
    @endforeach
  </div>
</div>

@endsection