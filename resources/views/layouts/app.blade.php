<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aplicación de prueba Haron Acosta - @yield('title')</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>

<body>
    @include('partials.header')
    <main class="main" role="main">
        @yield('content')
    </main>
    @include('partials.footer')


    <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>