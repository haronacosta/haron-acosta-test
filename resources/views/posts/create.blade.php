@extends('layouts.app')

@section('title', 'Crear Post')

@section('content')

<div class="container">
    <div class="h4 text-center my-5">
        Crear Post
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form class="form" action="{{ url('post') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="title" class="font-size-1">Titulo:</label>
                    <input type="text" class="form-control" id="title" placeholder="Titulo del post" name="title"
                        value="{{ old('title') }}">
                </div>

                <div class="form-group">
                    <label for="content" class="font-size-1">Contenido del post:</label>
                    <textarea class="form-control" id="content" rows="3" name="content">{{ old('content') }}</textarea>
                </div>

                <button type="submit" class="btn btn-post w-100">Guardar post</button>
            </form>
        </div>

    </div>

</div>

@endsection