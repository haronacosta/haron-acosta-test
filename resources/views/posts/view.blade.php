@extends('layouts.app')

@section('title', 'Post {{ $post["title"] }}')

@section('content')

<div class="container">

    <div class="h4 text-center my-5">
        {{ $post['title'] }}
    </div>

    <section>
        <div class="h6"><b>Autor: {{ $post['user']->name }}</b></div>
        @if ($post['user']->id == Auth::user()->id)
        <div class="text-right">
            <a class="btn btn-info my-3 text-white" href="{{route('edit', $post['id'])}}">Editar</a>
            <a class="btn btn-danger my-3 text-white" data-toggle="modal" data-target="#eliminateModal">Eliminar</a>
        </div>
        @endif

        <p>
            {{ $post['content'] }}
        </p>
    </section>

    <div class="comentarios my-4 pb-4">

        <div class="row justify-content-start">

            <div class="col-lg-6 ">
                <div class="alert alert-danger d-none" id="commentError"></div>
                <form name="commentForm" id="commentForm" data-url="{{ url('/comment')}}" data-id="{{$post['id']}}">
                    @csrf
                    <div class="form-group">
                        <label for="postContent" class="font-size-1">Agregar comentario:</label>
                        <textarea class="form-control" id="comment" rows="3"></textarea>
                    </div>

                    <button type="submit" class="btn btn-post w-100">Agregar</button>
                </form>


            </div>
        </div>

        <div class="col-lg-6 my-5">

            <div class="h5 font-weight-bold">Comentarios:</div>
            <ul class="list-unstyled" id="commentsContainer">
                @foreach ($comments as $comment)
                <li class="my-2">
                    <i class="fas fa-user"></i> <b></b> {{ $comment['user']->name }}
                    <p>{{$comment['comment']}}</p>
                </li>
                @endforeach

            </ul>
        </div>
    </div>

</div>

<div class="modal fade" id="eliminateModal" tabindex="-1" role="dialog" aria-labelledby="eliminateModalModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="eliminateModalModalLabel">¿Esta seguro de eliminar el post?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <form action="{{ route('delete', $post['id']) }}" method="POST" id="deletePost">@csrf @method('DELETE')
                </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger text-white"
                    onclick="javascript: document.getElementById('deletePost').submit()">
                    Eliminar
                </button>
            </div>
        </div>
    </div>
</div>

@endsection