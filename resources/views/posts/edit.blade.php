@extends('layouts.app')

@section('title', 'Editar Post')

@section('content')

<div class="container">
    <div class="h4 text-center my-5">
        Editar Post
    </div>

    <div class="row justify-content-center">
        <div class="col-lg-8">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form class="form" action="{{ route('update', $post['id']) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="postTitle" class="font-size-1">Titulo:</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$post['title']}}">
                </div>

                <div class="form-group">
                    <label for="postContent" class="font-size-1">Contenido del post:</label>
                    <textarea class="form-control" id="content" name="content" rows="3">{{$post['content']}}</textarea>
                </div>

                <button type="submit" class="btn btn-post w-100">Guardar post</button>
            </form>
        </div>

    </div>

</div>

@endsection