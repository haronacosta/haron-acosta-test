<header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-green">
        <a class="navbar-brand" href="{{ url('/') }}"> <b>Aplicación Haron Acosta</b></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav ml-auto">

                @if(Auth::check())
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Inicio<span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('create') }}">Crear un post</a>
                </li>

                <li class="nav-item dropdown">   
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i> {{ Auth::user()->name }}
                    </a>
                    <form method="POST" action="{{ route('logout') }}" id="logout">@csrf</form>

                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">   
                        <a class="dropdown-item pointer"
                            onclick="javascript: document.getElementById('logout').submit()">Cerrar sesión</a>
                    </div>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">Iniciar sesión</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">Registrarse</a>
                </li>
                @endif
            </ul>
        </div>
    </nav>

</header>