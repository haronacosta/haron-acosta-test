<footer class="mt-auto">
    <div class="container h-100">
        <div class="row h-100 align-items-end">
            <div class="col-lg-4 offset-8">
                <div class="text-right">
                    Haron Acosta &copy; 2020
                </div>
            </div>
        </div>
    </div>
</footer>