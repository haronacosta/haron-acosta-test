window.addEventListener(
    "load",
    function() {
        let comment = document.getElementById("commentForm");

        comment.addEventListener("submit", add_comment, false);
    },
    false
);

function add_comment(event) {
    event.preventDefault();

    let container_error = document.getElementById("commentError");

    container_error.classList.add("d-none");

    let url = event.target.dataset.url;

    let comment = document.getElementById("comment").value;

    let csrf = document.getElementsByName("_token").value;

    let post_id = event.target.dataset.id;

    axios
        .post(url, {
            _token: csrf,
            post_id: post_id,
            comment: comment
        })
        .then(function(response) {
            console.log();
            if (response.status === 200) {
                document.getElementById("comment").value = "";

                let container = document.getElementById("commentsContainer");

                let comments = response.data;

                while (container.firstChild) {
                    container.removeChild(container.firstChild);
                }

                comments.forEach(comment => {
                    console.log(comment);
                    let html = ` <li class="my-2">
                <i class="fas fa-user"></i> <b></b> ${comment.user.name}
                <p>${comment.comment}</p>
            </li>`;

                    container.insertAdjacentHTML("beforeend", html);
                });
            }
        })
        .catch(function(error) {
            let comment_error = error.response.data.errors.comment;

            let container_error = document.getElementById("commentError");

            container_error.classList.remove("d-none");

            container_error.insertAdjacentText("beforeend", comment_error);
        });
}
