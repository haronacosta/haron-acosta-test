<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
     /**
     * Get the post that owns the comment.
     */
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    /**
     * Gets the user who made this comment
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
