<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'post_id' => 'required',
            'comment' => 'required',
        ];

        $messages = [

            'post_id' => 'Falta el identificador del post',
            'comment.required' => 'Debe ingresar un comentario'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        $user = Auth::user();

        $user_id = $user->id;

        $comment = new Comment();

        $comment->post_id = $request->post_id;

        $comment->user_id = $user_id;

        $comment->comment = $request->comment;

        $comment->save();

        $comments = Post::find($request->post_id)->comments;

        foreach ($comments as $comment) {
            $comment['user'] = $comment['user'];
        }

        return response()->json($comments, 200);
    }
}
