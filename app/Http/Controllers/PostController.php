<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        return view('main', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create', ['name' => 'Haron Acosta']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required|max:200',
            'content' => 'required',
        ];

        $messages = [
            'title.required' => 'Debe ingresar un titulo para el post',
            'title.max' => 'El titulo no puede contener mas de 200 caracteres',
            'content.required' => 'Debe ingresar el contenido del post'
        ];

        $validator = Validator::make($request->all(), $rules, $messages)->validate();

        $user = Auth::user();

        $user_id = $user['id'];

        $post = new Post;

        $post->user_id = $user_id;

        $post->title = $request->title;

        $post->content = $request->content;

        $post->save();

        return redirect('/')->with('success', 'Post creado con exito!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        $comments = Post::find($id)->comments;

        return view('posts.view', compact('post', 'comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {



        $data = $request->all();

        $data['id'] = $id;

        $rules = [
            'id' => 'required',
            'title' => 'required|max:200',
            'content' => 'required',
        ];

        $messages = [
            'title.required' => 'Debe ingresar un titulo para el post',
            'title.max' => 'El titulo no puede contener mas de 200 caracteres',
            'content.required' => 'Debe ingresar el contenido del post'
        ];

        $validator = Validator::make($data, $rules, $messages)->validate();

        $post = Post::find($id);

        $post->title = $request->title;

        $post->content = $request->content;

        $post->save();

        return redirect('/')->with('success', 'Post actualizado con exito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        
        $post->delete();

        return redirect('/')->with('success', 'Post se elimino con exito!');
    }
}
