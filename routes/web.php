<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// POST 

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'PostController@index')->name('home');

    Route::get('/post/create', 'PostController@create')->name('create');

    Route::post('post', 'PostController@store');

    Route::get('/post/{post_id}', 'PostController@show');

    Route::get('/post/{post_id}/edit', 'PostController@edit')->name('edit');

    Route::put('post/{post_id}', 'PostController@update')->name('update');

    Route::delete('post/{post_id}', 'PostController@destroy')->name('delete');
});

// COMMENTS 

Auth::routes();

Route::post('comment', 'CommentController@store');
