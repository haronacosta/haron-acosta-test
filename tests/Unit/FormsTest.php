<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\User;

class FormsTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * Create Post Test
     *
     * @return $response
     */

    public function testCreatePost()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
            ->post('/post', [
                'title' => 'Post 1',
                'content' => 'Lorem ipsum dolor sit ame',
                'user_id' => 1
            ]);

        $response->assertRedirect('/');
    }

    /**
     * Update Post Test
     *
     * @return $response
     */

    public function testUpdatePost()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
            ->put('/post/1', [
                'title' => 'Update Post 1'
            ]);

        $response->assertRedirect('/');
    }

    /**
     * Create Comment Test
     *
     * @return $response
     */

    public function testCreateComment()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
            ->post('/comment', [
                'post_id' => '1',
                'comment' => 'Lorem ipsum dolor sit ame'
            ]);

        $response
            ->assertStatus(200);
    }
}
