<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RoutesTest extends TestCase
{
    /**
     * Test route base
     *
     * @return void
     */
    public function routeBase()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /**
     * Test route create post
     *
     * @return void
     */
    public function routeCreatePost()
    {
        $response = $this->get('/post/create');

        $response->assertStatus(200);
    }

    /**
     * Test route show post
     *
     * @return void
     */
    public function routeShowPost()
    {
        $response = $this->get('/post/1');

        $response->assertStatus(200);
    }

    /**
     * Test route edit post
     *
     * @return void
     */
    public function routeEditPost()
    {
        $response = $this->get('/post/1/edit');

        $response->assertStatus(200);
    }
}
