<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FormsTest extends TestCase
{
    /**
     * Create Post Test
     *
     * @return $response
     */

    public function testCreatePost() {
        $response = $this->post('/post', [
            'title' => 'Post 1',
            'content' => 'Lorem ipsum dolor sit ame',
            'user_id' => 1
        ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true
            ]);
    }

    /**
     * Update Post Test
     *
     * @return $response
     */

    public function testUpdatePost() {
        $response = $this->put('/post/1', [
            'title' => 'Update Post 1'
        ]);

        $response
            ->assertStatus(200)
            ->assert([
                'updated' => true
            ]);
    }

    /**
     * Create Comment Test
     *
     * @return $response
     */

    public function testCreateComment() {
        $response = $this->post('/comment', [
            'post_id' => '1',
            'comment' => 'Lorem ipsum dolor sit ame'
        ]);

        $response
            ->assertStatus(201)
            ->assert([
                'created' => true
            ]);
    }
}
